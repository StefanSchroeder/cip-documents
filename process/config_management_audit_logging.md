
# <Center> Configuration Management </Center>

# Table of contents
1. [Objective](#Objective)
2. [Acronyms](#Acronyms)
3. [Content](#Content)
4. [CIP Kernel version](#CIP_Kernel_version)
5. [CIP Core version](#CIP_Core_version)
6. [CIP Core change control ](#CIP_Core_change_control)
7. [CIP Kernel change control](#CIP_kernel_change_control)
8. [Audit Logging](#audit_logging)


   ***** 
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description                                                                                                      | Author       | Reviewed by                            |
|-------------|------------|-------------------------------------------------------------------------------------------------------------------------|--------------|----------------------------------------|
| 001         | 2022-09-21 | Draft mostly empty document                                                                                             | Dinesh Kumar | To be reviewed with Certification Body |
| 002         | 2023-01-20 | Added version section                                                                                                   | Dinesh Kumar | To be reviewed with Certification Body |
| 003         | 2023-08-23 | Incorporated BV feedback to add following information for CIP Core and CIP Kernel<br>applicant, reviewer, approver, log | Dinesh Kumar | BV members                             |
| 004         | 2023-09-04 | Incorporated CIP kernel WG member's feedback                                                                            | Dinesh Kumar | CIP Core WG members                    |
| 005         | 2023-09-12 | Incorporated CIP Core WG member's feedback                                                                              | Dinesh Kumar | TBR                                    |
| 006         | 2023-11-09 | Included audit logging process details                                                                                  | Sai Ashrith  | TBR                                    |







<div style='page-break-after: always'></div>

***

## 1. Objective <a name="Objective"></a>

The primary objective of this document is to define CIP development configuration management as per IEC-62443-4-1 SM-1 requirement.

While creating this document CIP members do not have clear understanding what all parts of configuration management should be covered and would be applicable to CIP, as a result this document will be revised after discussion with Certification Body.


## 2. Acronyms <a name="Acronym"></a>

| Acronyms | Details                       |
|----------|-------------------------------|
| CIP      | Civil Infrastructure Platform |
| CB       | IEC certification Body        |

## 3. Content <a name="Content"></a>

As per discussion with Certification Body, Configuration management for CIP can include version of both 
CIP Kernel and CIP Core as these are the two key components.

The version information should be consistently updated in this document. In case if the version is maintained in some other document, here reference can be provided.

## 4. CIP Kernel version <a name="CIP_Kernel_version"></a>

This section to be updated as soon as CIP kernel team confirms about CIP Kernel version maintenance policy.

Following CIP wiki pages describe about CIP Kernel versions.

[CIP Kernel Maintenance](https://wiki.linuxfoundation.org/civilinfrastructureplatform/start#kernel_maintainership) page describes about CIP kernel versions maintained.

[CIP Kernel Maintenance Process](https://wiki.linuxfoundation.org/civilinfrastructureplatform/cipkernelmaintenance) page describes steps to be followed by CIP Kernel developers.


## 5. CIP Core version <a name="CIP_Core_version"></a>

The isar-cip-core metadata's versioning and release policy are put into [this](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/blob/next/doc/README_version-release-policy.md) document.

## 6. CIP Core change control <a name="CIP_Core_change_control"></a>

**Applicant:** [isar-cip-core](https://gitlab.com/cip-project/cip-core/isar-cip-core) repository accepts changes from any developer via gitlab MRs or mailing lists whether the developer is CIP member or not. The preferred way of accepting patches is via CIP-DEV mailing list.

**Reviewer:** Once the patches are shared for review in mailing lists, anyone who is part of mailing lists can review and share feedback. 

**Approver:** Once all the comments are closed by the owner of the patch, maintainer approves the patches and all approved patches are moved to next branch of isar-cip-core, after sometime patches are moved from next to master branch.

**Log:** As all patches go through this process hence all evidence and logs are in mailing lists and gitlab commit history.

All changes in isar-cip-core can be categorized as below.

> **meta-data changes:** Any recipe changes because of version updates, or new Debian version support etc. 

## 7. CIP kernel change control <a name="CIP_kernel_change_control"></a>

**Applicant:** [CIP Kernel](https://gitlab.com/cip-project/cip-kernel/linux-cip) repository accepts changes from any developer via only mailing lists whether the developer is CIP member or not. While accepting the changes the prerequisite is all changes should be up-streamed or in other words Patches that are already reviewed/merged by Linus Torvalds are eligible for merging into CIP kernels.

The only way of accepting patches is via CIP-DEV mailing list. In addition, CIP members keep adding patches which are customization for their hardware, but the same process is followed even for CIP members customization where only patches which are reviewed/merged by Linus Torvalds are eligible for merging into CIP kernels.

**CIP Kernel Config changes:** The process of kernel config change is same as for CIP kernel patches described in above section.

**Reviewer:** Once the patches are shared for review in mailing lists, anyone who is part of mailing lists can review and share feedback. 

**Approver:** Once all the comments are closed by the owner of the patch, maintainer approves the patches and all approved patches are moved respective CIP kernel branch e.g. `linux-6.1.y-cip`.

Other CIP kernel branches can be found at [CIP kernel repository](https://gitlab.com/cip-project/cip-kernel/linux-cip).

**Log:** As all patches undergo through the above review process hence all evidence and logs are maintained in the mailing lists and gitlab commit history.

All changes in [CIP Kernel](https://gitlab.com/cip-project/cip-kernel/linux-cip) can be categorized as below.

> **Fixes for CVEs** "On regular basis, CIP kernel maintainer merge changes from `Greg Kroah Hartmann's` stable branches. stable branches contain fixes for bugs that are already fixed by Linus Torvald's mainline releases.

> **Changes for CIP members CIP reference hardware** Sometimes CIP members add changes in CIP kernel which are specific to hardware customization which are already up-streamed. However, this practice is not very regular.

## 8. Audit Logging <a name="audit_logging"></a>

The audit logging process followed is described below:

1. Every major change made in any process document in CIP is logged in the revision history section in that respective document.
2. That log also contans the time, the member who made the change and the reviewer of the change.
3. To get a detailed view of all the changes made in the development cycle, **git log** provides that information in all CIP related repositories.
