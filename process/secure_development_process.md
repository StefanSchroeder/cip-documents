# <Center>CIP Secure Development Process </Center>

## Table of contents
1. [Overview](#Overview)
2. [SM-1 Secure Development Process](#SEC_DEV_PROCESS)
3. [SM-2 Identification of Responsibilities](#RACI_MATRIX)
4. [SM-3 CIP Software version](#CIP_SW_VER)
5. [SM-4 CIP Developer Security Expertise](#DEV_SEC_EXPERTISE)
6. [SM-5 Process Scoping](#PROCESS_SCOPING)
7. [SM-6 File Integrity](#FILE_INTEGRITY)
8. [SM-7 Development Environment Security](#DEV_ENV_SECURITY)
    
    8.1 [Development Security](#DEV_SECURITY)
    
    8.2 [Production Time Security](#PROD_SECURITY)
    
    8.3 [Delivery Time Security](#DELIVERY_SECURITY)
    
9. [SM-8 Private Key Protection](#PRIV_KEY_PROT)
10. [SM-9 Security Risk for new or externally provided  
     components](#SEC_RISK_EXT_COMP)
11. [SM-10 Custom Developed Components from third party](#CUSTOM_DEVELOPMENT)
12. [SM-11 Security Issues Assessment](#SEC_ISSUES_ASSESSMENT)
13. [SM-12 Documented Checklist Review](#CHECKLIST_REVIEW)
14. [SM-13 Define Review frequency ](#REVIEW_FREQUENCY)
15. [SR-1, SR-3, SR-4 Product Security Context](#PRODUCT_SEC_CONTEXT)
16. [SR-2 Threat Model ](#THREAT_MODEL)
17. [SR-5 Security Requirements Review and Approval](#SECURITY_REQUIREMENTS_REVIEW)
18. [SD-1 Secure Design Principles](#SECURE_DES_PRINCIPLE)
19. [SD-2 Defense in depth design](#DEFENSE_IN_DEPTH)
20. [SD-3, SD-4 Security design review](#SECURITY_DESIGN_REVIEW)
21. [SI-1, SI-2 Security implementation review](#SECURITY_IMPLEMENTATION_REVIEW)
22. [SVV-1 Security requirement testing](#SECURITY_REQUIREMENT_TESTING)
23. [SVV-2 Threat Mitigation testing](#THREAT_MITIGATION_TESTING)
24. [SVV-3 Vulnerability testing](#VULNERABILITY_TESTING)
25. [SVV-4 Penetration testing](#PENETRATION_TESTING)
26. [SVV-5 Independence of testers](#INDEPENDENCE_TESTERS)
27. [DM-1 to DM-5 Receiving notifications of security issues](#NOTIFICATION_OF_SECURITY_ISSUES)
28. [DM-6 Periodic review of security defect management practice](#REVIEW_DEFECT_MANAGEMENT)
29. [SUM-1 Security Update Qualification](#SECURITY_UPDATE_QUALIFICATION)
30. [SUM-2, SUM-3 Security update documentation](#SECURITY_UPDATE_DOCUMENTATION)
31. [SUM-4 Security update delivery](#SECURITY_UPDATE_DELIVERY)
32. [SUM-5 Timely delivery of security patches](#TIMELY_DELIVERY_SECURITY_PATCHES)
33. [SG-1, SG-2 Product defense in depth](#PRODUCT_DEFENSE_IN_DEPTH)
34. [SG-3 Security Hardening guidelines](#SECURITY_HARDENING_GUIDELINES)
35. [SG-4 Security disposable guidelines](#SECURITY_DISPOABLE_GUIDELINES)
36. [SG-5 Secure operation guidelines](#SECURITY_OPERATION_GUIDELINES)
37. [SG-6 Account management guidelines](#ACCOUNT_MANAGEMENT_GUIDELINES)
38. [SG-7 Documentation Review](#DOCUMENTATION_REVIEW)
39. [References](#REFERENCES)
41. [Further Pending Items](#PENDING_ITEMS)






   ***** 
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description                                                | Author       | Reviewed by              |
|-------------|------------|-------------------------------------------------------------------|--------------|--------------------------|
| 001         | 2021-03-14 | Draft secure development process                                  | Dinesh Kumar | TBR                      |
| 002         | 2021-08-12 | Added about CVE tracking process in CIP kernel and CIP Core       | Dinesh Kumar | TBR                      |
| 003         | 2021-09-03 | Added reference for File Integrity document                       | Dinesh Kumar | TBR                      |
| 004         | 2022-08-01 | Updated SM-11 with additional information                         | Dinesh Kumar | TBR                      |
| 005         | 2023-10-12 | Update SM-3,SM-7 and SM-11 details                                | Sai Ashrith  | TBR                      |
| 006         | 2023-11-10 | Update process realted details for SM-3                           | Sai Ashrith  | TBR                      |
| 007         | 2024-03-25 | Updated SG requirement details                                    | Sai Ashrith  | Dinesh Kumar, BV members |
| 008         | 2024-03-25 | Update SVV-5                                                      | Tsukasa Yobo | TBR                      |
| 009         | 2024-04-05 | Update SVV-1, 2 & 4                                               | Tsukasa Yobo | TBR                      |
| 010         | 2024-05-22 | Updated based on BV feedback to remove TODOs from SG requirements | Dinesh Kumar | SWG members              |
| 011         | 2024-05-27 | Updated SM-4 requirement process based on BV feedback             |              | SWG members              |


****
<div style='page-break-after: always'></div>

















<div style='page-break-after: always'></div>

### 1. Overview <a name="Overview"></a>

This document is based on IEC-62443-4-1 (Edition 1.0 2018-01) secure development process requirements.The Objective is to adhere IEC-62443-4-1 secure development process requirements in CIP development as much as possible.

Adherence to these secure development practices will give an edge to CIP over other distributions at the same time it will reduce IEC-62443-4-x development effort for CIP member companies for making products based on CIP.

### 2. [SM-1] Secure Development Process <a name="SEC_DEV_PROCESS"></a>

The development process details of CIP are provided in [this](./CIP_development_process.md) document.

### 3. [SM-2] Identification of Responsibilities <a name="RACI_MATRIX"></a>

CIP has defined roles and responsibilities for the members who are responsible for CIP development. This RACI(Responsible, Accountable, Consulted and informed ) is reviewed and updated yearly once or whenever there is change in responsibilities

Detailed RACI MATRIX is available at [CIP RACI matrix page](https://gitlab.com/cip-project/cip-documents/-/blob/master/process/raci.md).

### 4. [SM-3] CIP Software version <a name="CIP_SW_VER"></a>

The process flow while defining applicable software version is mentioned below:

1. CIP SWG members had a lot of internal meetings to decide the Debian and Kernel version applicable for IEC-62443-4-1 & 2 certification.
2. Each SWG member comes up with a proposal to define the applicable software ( Debian & CIP Kernel ) for the assessment.
3. After finalizing the decision based on majority vote, SWG members prepared a proposal to produce infront of TSC WG members.
3. SWG members include pros and cons in their proposal and finally TSC WG agreed upon Debian 12 (Bookworm) as the version for the base layer of CIP-Core component and 6.1.x version for the CIP Kernel for IEC-62443-4-1 and 4-2 certification.

### 5 [SM-4] CIP Developer Security Expertise <a name="DEV_SEC_EXPERTISE"></a>

CIP has multiple working groups. when a new member joins any CIP working group, its respective CIP member who is responsible to provide basic training and about CIP. Over the years, CIP members have developed various documents and material which is helpful to provide a quick overview of CIP to any new member.

When new member joins specific working group, it's working group chair responsibility to share following information with the new member.

* Details of how working group operates like meeting schedule etc.
* Details of gitlab repositories used for development or testing
* Providing required permissions to access repositories or contributions

As part of SWG above mentioned process is followed to on board new member. If a member is supposed to work for specific roles he/she is provided with required privileges.

SWG members maintain a template [Security Expertise document](https://drive.google.com/file/d/1SnUCRPeoZa3uJCwuapvF5tQjP1SLKaFl/view?usp=drive_link) where following information need to be provided by each member to understand the security expertise, past experience and suitability to assign a specific roles.

**Note** Security expertise document is a private document and is only accessible to CIP SWG members.

### 6 [SM-5] Process Scoping <a name="PROCESS_SCOPING"></a>

Following three documents can be used to list met and unmet requirements by CIP.

1. exida gap assessment [report](https://gitlab.com/cip-project/cip-security/iec_62443-4-x/-/tree/master/gap_assessment)

2. Secure development process document which is current document

3. Application and hardware guidelines document which describes about [IEC-62443-4-2](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/iec62443-app-hw-guidelines.md)

### 7. [SM-6] File Integrity <a name="FILE_INTEGRITY"></a>

Following document explains about CIP File Integrity and how user can verify integrity of CIP deliverables.

[About CIP File Integrity](https://gitlab.com/cip-project/cip-documents/-/blob/master/process/file_integrity.md)

### 8. [SM-7] Development Environment Security <a name="DEV_ENV_SECURITY"></a>

#### 8.1 Development Security <a name="DEV_SECURITY"></a>

Development environment security is achieved by providing restricted privileges to developers as well as all developers use certificate based authentication used by Gitlab.

#### 8.2 Production Time Security <a name="PROD_SECURITY"></a>

**This requirement is not applicable to CIP.**

#### 8.3 Delivery Time Security <a name="DELIVERY_SECURITY"></a>

CIP does not deliver any products to its customers. Instead the meta-data as well as the Kernel can be downloaded by the CIP users using Git cloning mechanism. Security during this phase is provided by Git security protocols, file integrity checks when the user tries to get the product from the respective Git repositories. During release, the commit hash is provided as an identifier to help the users get access to precise release made by the CIP members without any integrity issues.

Details of CIP development environment security is provided in [CIP Development Environment Security](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/development_environment_security.md?ref_type=heads) Document.

### 9. [SM-8] Private Key Protection <a name="PRIV_KEY_PROT"></a>

CIP maintains document which explains about CIP Private key management. Please refer following document for more details.

[CIP Private Key Management](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/private_key_management.md)

### 10. [SM-9] Security Risk analysis for externally provided components <a name="SEC_RISK_EXT_COMP"></a>

Since all components in CIP are developed externally, hence doing risk assessment for all components is not feasible. CIP team decides which components may pose risk to CIP platform. If a component is suspected to have known vulnerabilities then risk assessment is carried out by following below mentioned steps.

* Evaluate the open security issues/CVEs
* Categorize open CVEs from CIP perspective as low/medium/high
* If open CVEs fall in medium and high categories, do a threat modeling of the component and  
  decide the mitigation
* In addition CIP users should be notified for the vulnerable component via email notification

CIP Security, Kernel, Core, Software update working groups are responsible for carrying out such risk analysis for externally provided components. Based on working group recommendation and risk analysis, CIP TSC members make final decision by following voting process whether the component should be included in CIP.

In general any component which is not taken from Debian repository will undergo this process.

### 11. [SM-10] Custom Developed Components from third party<a name="CUSTOM_DEVELOPMENT"></a>

**This requirement is not applicable to CIP.**
This is applicable to end products.

### 12. [SM-11] Security Issues Assessment <a name="SEC_ISSUES_ASSESSMENT"></a>

 SM-11 requirement expects process followed for security issue handling during following phases.
 
* Requirements
* Secure by design
* Implementation
* Verification/Validation
* Defect Management

 As in CIP primary work is integration of components hence, CIP does not have any process to address security issues during all the above phases.
 
 CIP completely relies on Debian upstream and mainline linux kernel for security issue fixes and 
 bug tracking. CIP follows upstream first policy and all security issue fixes are first submitted to upstream. Even CIP member companies are advised to directly report issues and submit fixes to upstream projects.
 
 However, CIP uses open source vulnerability scanner and open source databases for security issues and identifying and sharing open CVE details with CIP users.
 
 CIP users should check the CVE list and decide which CVEs may impact their product security.
 
 In order to meet this requirement, CIP users are advised to take following actions.
 
 * Regularly review CVE list shared in CIP-DEV ML
*  If any CVE is critical for the product, do a risk assessment or wait for the fix to be 
   available
*  Overall, ensuring no critical security issues which may compromise product security leak to end users.

 CIP uses open source vulnerability scanner and open source data bases for security issues.
 Refer this [CIP CVE handling](./cve_handling.md) document which explains how CIP maintains CVE related information regarding the CIP-Core components using tools like **debsecan** and Kernel related CVEs from sources like upstream CVE scanner repositories and the process involved in sharing these results to the CIP users.
 
 In addition, CIP specific minimal issues are tracked at following two places.
 
* [CIP Core bug tracking](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/issues)

* [CIP Kernel bug tracking](https://gitlab.com/groups/cip-project/cip-kernel/-/issues)


### 13. [SM-12] Documented Checklist Review <a name="CHECKLIST_REVIEW"></a>
 
 CIP will need a documented checklist for tracking security practices defined by IEC62443-4-1 along with a documented process for ensuring the checklist is reviewed and updated for each release
 
 TODO: This needs to be further discussed within CIP members, how to address this requirement. 

### 14. [SM-13] Define Review frequency <a name="REVIEW_FREQUENCY"></a>
 
 All development process artifacts should be reviewed once in a year.
 The review should cover following items and review comments and observations should be documented based on IEC-62443-4-1 requirements of review evidence.
 1. Issues in current development process.
 2. Any critical issues reported by CIP members
 3. Actions to be taken to improve on points #1 and #2

 
### 15. [SR-1, SR-3, SR-4] Product Security Context <a name="PRODUCT_SEC_CONTEXT"></a>
 
 CIP generic security context has been defined in Security Requirement document.
 The Security Context would be revised as and when new deployment scenarios and requirements are found.
 
 [CIP Security Requirements](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/security_requirements.md)
 
### 16. [SR-2] Threat Model <a name="THREAT_MODEL"></a>
 
 CIP generic Threat Model has been created which defines the condition of Threat Model Review and update frequency.
 
 Threat Model document is available at [CIP Threat Model document](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/threat_modelling.md)
 
### 17. [SR-5] Security Requirements Review and Approval <a name="SECURITY_REQUIREMENTS_REVIEW"></a>
 
 Security requirements should be reviewed and approved when created. All review comments should be documented. During review following members should be invited.
 
 *  Architects/developers (those who will implement the requirements)
 *  Testers (those who will validate that the requirements have been met)
 *  Customer advocate (such as sales, marketing, product management or
    customer support) and Security Adviser
	 
### 18. [SD-1] Secure Design Principles <a name="SECURE_DES_PRINCIPLE"></a>
 
  1. The design shows how the system's devices and subsystems are connected, and    how external actors are connected to the system.
   
  2. The design shows all protocols used by all external actors to communicate  
     with the system.
   
  3. Trust boundaries are documented.
   
  4. The design document should be updated whenever the design changes
  
  The above mentioned details are documented [here](./CIP_secure_design.md).

### 19. [SD-2] Defense in depth design <a name="DEFENSE_IN_DEPTH"></a>
 
 **This requirement is not applicable to CIP.**
 This should be met by end product owners.
 Defense in depth design should be created by end product owners as it depends upon end products design and what kind of security layers would be part of the defense layers.
 
### 20. [SD-3, SD-4] Security design review <a name="SECURITY_DESIGN_REVIEW"></a>
 
 * Create evidence for security design reviews
 * Issues identified during security design reviews are tracked using gitlab
 * Create Traceability matrix for security requirements to security design
 * Create Traceability matrix from threat mitigation to security design
 * Create Security guidelines for user
 * Include security design best practices used in debian as well as review   
   Design best practices being developed by OpenSSF if suitable include in CIP

 Details regarding security design review and best practices in CIP are documented [here](./secure_design_review_bestpractices.md) based on above checklist.

### 21. [SI-1, SI-2] Security implementation review <a name="SECURITY_IMPLEMENTATION_REVIEW"></a>
 
 * Document Debian secure coding guidelines
 * Perform static code analysis or re-us from upstream for critical packages
 * Code reviews should document following information
 
	  1. Name of the person who performed the code review,
	  2. The date of the code review,
	  3. The results of the code review
	  4. The name of the person responsible for fixing problems identified in the    code   review
	  5. Date or indication that all problems were fixed.
 
### 22. [SVV-1] Security requirement testing <a name="SECURITY_REQUIREMENT_TESTING"></a>
 
 CIP Security requirements testing should be done following Items
 
 a) Functional testing of security requirements
 b) Performance and scalability testing
 c) Boundary/edge condition, and stress and  
   malformed or unexpected input tests not specifically targeted at security.
 d) Trust boundary requirements testing

### 23. [SVV-2] Threat Mitigation testing <a name="THREAT_MITIGATION_TESTING"></a>
  
  Threat mitigation testing should be done following step.
  1)Make the list of implemented mitigation to address a specific threat
  2)Make the test plan whether the mitigation works as designed.
  3)Execute the test according to the test plan.

### 24. [SVV-3] Vulnerability testing <a name="VULNERABILITY_TESTING"></a>
  
  Vulnerability scanner and Pen testing Tool should be used and following testing should be done.
  
  * Network storm testing should be done to simulate DOS attacks
  * Software composition analysis must be done against the binaries
  * Attack surface analysis
  * Known vulnerability scanning
  * Dynamic Runtime Resource Analysis Testing
  * Fuzz testing on all protocols sent externally should be included as part of   this testing
  
### 25. [SVV-4] Penetration testing <a name="PENETRATION_TESTING"></a>
  
  Penetration testing is performed by an outside professional organization. This is because it requires special testing tools and expertise.

  
### 26. [SVV-5] Independence of testers <a name="INDEPENDENCE_TESTERS"></a>
  
 Routine testing will be performed by the CIP Testing Working Group, which is "independent" of the upstream project developers and CIP kernel maintainers.
  
### 27. [DM-1 to DM-5] Receiving notifications of security issues <a name="NOTIFICATION_OF_SECURITY_ISSUES"></a>
  
  Security issues are tracked using CVE scanner tools for both CIP Kernel and CIP Core.
  
  CIP CVE scanner runs periodically to fetch fixes for CVEs and apply in CIP repos.

  Further details can be found about CIP Core CVE scanner at [CIP Core CVE scanner](https://gitlab.com/cip-playground/cip-core-sec)
  
  **CIP Kernel CVE Scanner**
  
  CIP Kernel CVE checking is done weekly and reports are published in cip-dev mailing list.) Currently there is no specific policy to stop releasing because of missing patches as long as stable kernels are released
  
  Release policy is reported at every E-TSC. The latest one is as follows
  [CIP Kernel CVE fixes release policy](https://docs.google.com/presentation/d/12cP80przQxXkzj2fAUptnieHar0jrB00vzIePkh5kwo/edit#slide=id.g9f78cf691e_0_155)
  
  Further details of CIP Kernel CVE scanner can be found at [CIP Kernel CVE scanner](https://gitlab.com/cip-project/cip-kernel/cip-kernel-sec)
  
  Notification of CVE fixes are sent by email to CIP users.
  
  CIP does not maintain it's own bug tracking system. Refer this [document](Security_issues_handling.md) to see the upstream methods to handle the CVE cycle.
  
### 28. [DM-6] Periodic review of security defect management practice <a name="REVIEW_DEFECT_MANAGEMENT"></a>
  
  Review current defect management practices and processes once in a year and make required changes as needed.
  
### 29. [SUM-1] Security Update Qualification <a name="SECURITY_UPDATE_QUALIFICATION"></a>
  
  CIP can not produce evidence for details of testing each patch or security issues. The verification information is not kept at any central location, it's scattered at multiple locations such as KernelCI, LAVA as well as mailing list.
  
  CIP kernelCI reports are available at [Kernel CI page](https://linux.kernelci.org/job/cip/) 

  CIP has LAVA automated tests which are executed when some code changes are merged. At the moment there is no tests for confirming side effects.
  
  CIP users should meet this requirement based on the product requirement and frequency of updates needed etc.
  
### 30. [SUM-2, SUM-3] Security update documentation <a name="SECURITY_UPDATE_DOCUMENTATION"></a>
  
  CIP will continue to use mailing as main channel for sharing security issues information with all users.
   
### 31. [SUM-4] Security update delivery <a name="SECURITY_UPDATE_DELIVERY"></a>
  
  CIP releases patches, CIP kernel and CIP Core meta-data which are signed by CIP developers.
  
  
### 32. [SUM-5] Timely delivery of security patches <a name="TIMELY_DELIVERY_SECURITY_PATCHES"></a>
  
  TBD: Document the frequency of CIP releases.
  
### 33. [SG-1, SG-2] Product defense in depth <a name="PRODUCT_DEFENSE_IN_DEPTH"></a>
  
  `SG-1` and `SG-2` requirements are not applicable to CIP as CIP users may develop various types of products using CIP platform (e.g. host device, network device), therefore this requirement should be filled by CIP end product not by CIP platform. 
  
### 34. [SG-3] Security Hardening guidelines <a name="SECURITY_HARDENING_GUIDELINES"></a>
  
  a. Every user created by a CIP user/administrator in their product has certain access privileges based on the default security context of the system. **acl** utility provided in CIP can be used by the admin to alter these access controls for certain user to protect some confidential data or process.

  b. Instructions to follow while integrating product’s application programming interfaces/protocols with user applications should be done by the end product's owner.

  c. Instructions to apply and maintain the product's defense in depth strategy should be documented by the end product's owner.

  d. In CIP there are some pre-defined policies regarding security capabilities like password strength, locking an account if multiple incorrect login attempts are detected, immediate intimation if audit logging failures are detected in the system, remote login policies etc. These settings are already pre-configured [here](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/tree/master/recipes-core/security-customizations?ref_type=heads) but based on product owner's use-case they can be modified to meet their requirement.

  * Contribution of these security capabilities provided by CIP to the product's defense in depth strategy should be judged by the product owner.

  * But in a general scenario, let us assume an attacker trying to perform a login to the running system with some random passwords. If he does this for a certain number of times which is configurable, the account gets locked. Here, the password strength and number of incorrect login attempts set by the user will acts as a firewall. If this is crossed by that attacker, then access controls provided to the files that user wants to protect can act as a second firewall. If this is also crossed by the attacker, then the intrusion detection system provided by **aide** shall act as an alarm to the user in taking immediate course of action.

  * These configurable values can be set/changed even in the running system the administrator.

  e. More than 20 Debian [security packages](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/tree/master/recipes-core/security-customizations?ref_type=heads) are installed to provide a certain set of security capabilities to the end product. These tools can be used for administration, monitoring, incident handling by the user. 

  f. CIP's two main components **CIP-Core** and **CIP-Kernel** are being maintained regularly. So it is the product user's responsibility to actively follow latest security fixes provided to CIP Kernel and Debian packages installed in the system. The user has to update their system to bring in the latest fixes. Refer **REQ-CIP-HARD-007** & **REQ-CIP-HARD-008** in [hardening document](../security/CIP_Security_Hardening.md) for more information.

  g. Any vulnerabilities in CIP Kernel shall be provided on a weekly basis. It is the end user's  responsibility to subscribe and get the latest information on Kernel CVE's. CIP provides a [tool]((../security/CIP_Security_Hardening.md)) which can be used to get the active CVEs of all the Debian packages installed in the system. These security incidents need to be properly tracked by the product supplier and should be reported to the end user.

  Additionally CIP has also defined detailed security hardening guidelines for following.

  * Default security policies to meet IEC-62443-4-1 security requirements
  * Compilation flags
  * Other configs

  Those details can be found in this [CIP security hardening](../security/CIP_Security_Hardening.md) document.
  
### 35. [SG-4] Secure Disposal Guidelines <a name="SECURITY_DISPOABLE_GUIDELINES"></a>
  
  **This requirement is not applicable to CIP.**
  
  Disposable guidelines are applicable to end products.

  Some general guidelines for secure disposal of equipment are mentioned below for reference.

  a. Users must ensure that process of equipment disposal is strictly controlled or else it may impact the confidentiality of data making it available to unauthorized audience.

  b. Devices that contain sensitive information should be physically destroyed or the information must be destroyed, deleted or overwritten using techniques that make the original data non-retrievable.

  c. If the equipment is going to be re-used it is important any previous data and potentially installed software is securely wiped and the device is returned to a known "clean" state.

  d. As an additional layer of security, the information can be encrypted before disposal. In this way, a hypothetical case that someone could recover the information through some mechanism, then decryption is required.
  
  There are various security compliance based on the type of device and related domain, CIP users are advised to refer specific compliance details for media sanitization. NIST published media sanitization standard [NIST SP.800-88r1](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-88r1.pdf) is one of the most popular standard.
  
### 36. [SG-5] Secure operation guidelines <a name="SECURITY_OPERATION_GUIDELINES"></a>
  
  As a platform CIP does not provide any secure operation guidelines as guidelines depend upon specific type of product.
  In general, secure operation guidelines include certificate, password management and authentication mechanisms, assigning different privileges to different users and mapping them to specific roles.
  
  As a platform CIP has IEC layer which can be configured for specific security requirements. Some of the default security configurations provided in CIP security image are following.

  This policy enforces user creation with strong password. [policies](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/blob/master/recipes-core/security-customizations/files/postinst?ref_type=heads#L15).

  Account will be locked if consecutive unsuccessful login attempts are made. All transactions are recorded in the audit logs and any failure in audit logging shall be reported to the user. [Multi-factor](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/blob/master/recipes-core/security-customizations/files/postinst?ref_type=heads#L15) authentication mechanism is configured in CIP.
  
  aide tool installed in the system provides intrusion detection functionality. Administrator needs to run the aide check to monitor any integrity failures in the system.
  
  [CIP user security manual](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/user_security_manual.md?ref_type=heads) provides details of how to configure CIP IEC layer to enforce specific security policy.
  
  ### 37. [SG-6] Account management guidelines <a name="ACCOUNT_MANAGEMENT_GUIDELINES"></a>
    
  CIP security image has only one default user account which is root account with the password root. 
  For creating additional user accounts, follow standard linux user management steps. 
  
  CIP end users should decide about how many additional users should be created based on their requirements. Here we are providing few best practices which will help to improve security of the system.
  
* Monitor and audit each user account
* Follow least privilege principal
* Implement strong password policies
* All user accounts should be reviewed periodically to ensure that they are still necessary and being used appropriately.
* Inactive users should be removed from the system, while active users should be monitored for suspicious or unusual behavior.

  
    
### 38. [SG-7] Documentation Review <a name="DOCUMENTATION_REVIEW"></a>
   
  All CIP development documents are maintained in gitlab. Reviewers can give comments using any of the following methods.
  
  * Create gitlab issues with comments
  * Send review comments in email
  * Send MR with the changes
  
  Only few CIP members have rights to merge review comments changes in the documents.
  
