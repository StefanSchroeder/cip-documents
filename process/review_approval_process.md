# <Center>Roles and Responsibilities</Center>

# Table of contents
1. [Objective](#Objective)
2. [Scope](#Scope)
3. [Review & Approval Process](#review_approval)

    3.1 [Documents](#documents)
    
    3.2 [Code](#Code)
    
    3.3 [Other Artifacts](#other)


   ***** 
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description                                          | Author       | Reviewed by |
|-------------|------------|-------------------------------------------------------------|--------------|-------------|
| 001         | 2024-01-29 | Draft review & approval process for CIP development records | Dinesh Kumar | Stefan Schroeder     |
| 002         | 2024-05-23 | Fixed review comments provided by Stefan                    | Dinesh Kumar | Stefan Schroeder         |

****
<div style='page-break-after: always'></div>


<div style='page-break-after: always'></div>

***

## 1. Objective <a name="Objective"></a>

Objective of this document is to explain the review process of all CIP-documents stored in the CIP-project's Gitlab repository.

Objective of this document is to define repeatable process for CIP development process records.
CIP being a platform based on Open Source Components where multiple developers contribute who come from different backgrounds, hence the process is generic and tries to use various tools for review and approval.

## 2. Scope <a name="Scope"></a>

The scope of this document is artifacts, like code, scripts, documents, developed by all CIP work-group members

Scope of this document is to meet the following IEC-62443-4-1 security process requirements:

SR-5: Security requirements review
SD-3: Secure design review
SI-1: Security implementation review

## 3. Review and Approval Process <a name="review_approval"></a>

CIP produces various types of artifacts and depending upon the type of artifact review and approval processes vary.

Some of the artifacts like workgroup meeting agenda, general technical discussion etc don't undergo any review and approval process.

The following sections define the review and approval process followed by CIP working groups for development artifacts

### 3.1 Documents <a name="documents"></a>

  All development process documents have a revision history. Changelogs are maintained indefinitely. Changeloge include modification date and creation date, reviewer, etc. All documents are maintained in Gitlab. This guarantees the availability of complete, correct and consistent changelogs. Changelogs are derived using the "git log" functionality.
  
  Following are the steps followed for review and approval of development documents.
  
  **Step-1** Any CIP member can create draft documents and share it for CIP members review via Gitlab merge requests (MR).
  
  **Step-2** Related members share feedback and the owner of the document resolves all comments.
  
  **Step-3** Once all comments are resolved the maintainer of the cip-documents repository will merge the document, once the document is merged with no pending review comments, it is considered as approved by the reviewer.
  
  Any further change follows same process and revision history updates.


NOTE: The cip-documents repository is configured to prevent direct commits to the "master-branch". (See https://docs.gitlab.com/ee/user/project/protected_branches.html) All changes must be presented in the form of "Merge Requests" that need to be approved. (See https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow)


#### Representation

All documents are processed to PDF for easier consumption by the reader using the built-in Gitlab CI-CD-infrastructure.
(See https://docs.gitlab.com/ee/ci/)

The PDF versions of documents can be downloaded from the 

https://gitlab.com/cip-project/cip-documents/-/pipelines

page of the repository.

### 3.2 Code <a name="Code"></a>

CIP reuses upstream components and integrates them for creating CIP reference images. CIP members write very minimal code, scripts and configuration. All types of code changes follow the process outlined below.

**Step-1** Anyone can create patches in different CIP workgroups and share for CIP members review via mailing lists.

**Step-2** Members of the respective mailing lists review the changes and share feedback, contents of mailing lists are             archived. 

**Step-3** Owner of the patch completes rework to fix all review comments and makes required changes in the patch. Once all review                   comments are resolved, respective maintainer of the repository accepts the patches. However, in case if the owner of the patch 
           and maintainer is same, patch owner can merge his patches as all maintainers are experienced members and explain to working group members.

All the change log history is maintained in Gitlab commit logs.

### 3.3 Other Artifacts <a name="other"></a>

CIP produces several other types of artifacts like working group agenda documents, investigation documents, presentations at various Open Source Software (OSS) events etc. These documents don't follow any review and approval process.