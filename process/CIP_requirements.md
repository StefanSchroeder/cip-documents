# CIP Requirements

## Table of contents

1. [Introduction](#introduction)
2. [Process of defining CIP Requirements](#process)
3. [CIP Functional Requirements](#cipfunctionalrequirements)
4. [CIP Non-functional Requirements](#cipnonfunctionalrequirements)
5. [CIP Security Requirements](#securityrequirements)

  ***** 
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description                                 | Author       | Reviewed by  |
|-------------|------------|----------------------------------------------------|--------------|--------------|
| 001         | 2023-01-04 | Template document for CIP requirements             | Sai Ashrith  | Dinesh Kumar |
| 002         | 2023-03-08 | Add CIP functional and non-functional requirements | Dinesh Kumar |              |
| 003         | 2023-07-10 | Updated requirement IDs based on BV feedback.      | Dinesh Kumar | TBR          |
| 004         | 2023-11-10 | Add process details while defining requirements    | Sai Ashrith  | TBR          |
****

## Introduction <a name="introduction"></a>

This document is intended to define and document CIP requirements as a platform.There are generic CIP platform requirements which are mainly derived from [CIP white paper](https://www.cip-project.org/wp-content/uploads/sites/17/2018/10/CIP_Whitepaper_10.19.18.pdf). 

IEC-62443-4-1 SM-1 expects the component to have defined requirements which can be tested. The requirements can be functional, non-functional, performance, security etc.

The basic goals of CIP have been documented in a whitepaper available at [CIP project portal](https://www.cip-project.org/wp-content/uploads/sites/17/2018/10/CIP_Whitepaper_10.19.18.pdf). According to the Certification Body the goals defined in the CIP whitepaper are quite abstract and cannot be considered to meet IEC-62443-4-1 Secure Development Process requirement.

## Process of defining CIP Requirements <a name="process"></a>

The process flow while defining CIP requirements is mentioned below:

1. When CIP is decided to be built as a platform, workgroups like TSC, CIP Core, CIP Kernel and security had numerous meetings to define a set of requirements which are platform based.
2. In every joint WG meeting, respective WG comes up with a proposal for a specific requirement.
3. All the WG members provide their opinion and vote whether to accept that proposal or reject it based on the scope and available expertise.
4. Finally, to build a platform on which numerous end-user applications can be developed, various features like **SLTS maintained kernel (RT & non-RT)**, **Multi-Architecture support**, **Security features based on IEC 62443-4-2 standard** are finalized and announced as CIP requirements.

## CIP Functional Requirements <a name="cipfunctionalrequirements"></a>

| S No. | Requirements | Details | Responsible WG |
|----|----|----|----|
| #REQ-CIP-FUNC-01 | Re-use Linux mainline kernel, customise configs based on CIP members requirement | CIP to reuse Linux mainline kernel                                                                                                                                 | CIP Kernel                         |
| #REQ-CIP-FUNC-02 | Provide CIP RT kernel by applying PREEMPT_RT patches                             | CIP to maintain its own RT kernel                                                                                                                                  | CIP Kernel                         |
| #REQ-CIP-FUNC-03 | Develop meta-data to create minimal CIP reference images                         | Create recipes and meta-data to re-use Debian packages for creating minimal CIP reference image                                                                    | CIP Core                           |
| #REQ-CIP-FUNC-04 | Support multiple cpu architectures in CIP reference images                       | Recipes and meta-data should be configurable to support multiple architectures such as amd64, arm64, armhf                                                         | CIP Core, CIP Kernel               |
| #REQ-CIP-FUNC-05 | Support Secure boot                                                              | Support secure boot with or without secure storage                                                                                                                 | CIP Core, CIP Kernel               |
| #REQ-CIP-FUNC-06 | Support SWUpdate with local file and OTA                                         | CIP users should be able to update devices using local file using sdcard or eMMC or using OTA updates                                                              | CIP SWUpdate                       |
| #REQ-CIP-FUNC-07 | Support SWUpdate with signed & encrypted images                                  | CIP should support SWUpdate with Signed and Encrypted images                                                                                                       | CIP SWUpdate                       |
| #REQ-CIP-FUNC-08 | CIP Security detailed requirements are documented in a separate document at      | https://gitlab.com/cip-project/cip-documents/-/blob/master/security/security_requirements.md                                                                       | CIP SWG & CIP Core                 |
| #REQ-CIP-FUNC-09 | Deliver a generatable SBOM along with the sample configuration                   | The CIP packages, the tooling to create the packages and system image for the reference hardware shall be enabled to also provide a SBOM for the provided software | CIP Core, CIP Kernel, CIP SWUpdate |

## CIP Non-Functional Requirements <a name="cipnonfunctionalrequirements"></a>

| S No. | Requirements | Details | Responsible WG |
|----|----|----|----|
| #REQ-CIP-NON-FUNC-01 | Follow upstream first policy for CIP Core and CIP Kernel development                      | CIP members to follow upstream policy for the issue fixes in CIP Kernel or CIP Core should be first upstreamed before accepting in CIP                     | CIP Kernel     |
| #REQ-CIP-NON-FUNC-02 | Maintain SLTS kernel for 10+ years                                                        | CIP members to decide democratically SLTS kernel and maintain for up to 10 years by providing security fixes and updates to CIP users                      | CIP Kernel     |
| #REQ-CIP-NON-FUNC-03 | Use Debian based packages or third party applications to create CIP Core reference images | The primary source of CIP Core packages is Debian repositories. However, some packages may also come from other repositories based on all members decision | CIP Core       |
| #REQ-CIP-NON-FUNC-04 | Accept only kernel patches which are upstreamed                                           | CIP Kernel maintainers to ensure all the patches applied in the CIP kernel are from stable upstream trees                                                  | CIP Kernel     |

## CIP Security Requirements <a name="securityrequirements"></a>

As CIP did not have any clearly defined security requirements hence CIP Security requirements have been taken from IEC-62443-4-2 in order to add security capabilities.

CIP Security requirements are documented at [CIP Security Requirements](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/security_requirements.md)
