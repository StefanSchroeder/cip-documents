# Traceability from CIP requirements to design and testing

## Table of contents
1. [Introduction](#introduction)
2. [Scope](#scope)
3. [Acronyms](#acronyms)
4. [Process behind creating requirement traceability matrices](#process)
5. [Traceability matrix from CIP requirements to design](#reqtodesign)
6. [Traceability matrix from CIP requirements to testing process](#reqtotesting)

*****
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description                                            | Author       | Reviewed by                               |
|-------------|------------|---------------------------------------------------------------|--------------|-------------------------------------------|
| 001         | 2023-09-15 | Draft document for traceability from CIP requirements to design and testing process   | Sai Ashrith |  TBR               |
| 002         | 2023-10-10 | Updated design and test descriptions by adding references to user manual document | Sai Ashrith  |     TBR               |
| 003         | 2023-11-09 | Added details regarding process involved in making the traceability matrices      | Sai Ashrith  |     TBR               |




<div style='page-break-after: always'></div>

*****

### Introduction <a name="introduction"></a>

This document shows the traceability from CIP functional and non-functional requirements to the respective design and testing process followed by the CIP WG members to fulfill those requirements.

### Scope <a name="scope"></a>

This document does not show the traceability from CIP security requirements to design and testing because it is already documented for IEC 62443-4-2 requirements in [security](../security) section in this repository.

### Acronyms <a name="acronyms"></a>

| S No.  | Acronym | Definition                                | 
|--------|---------|-------------------------------------------|
| 1      | WG      | Workgroup                                 |
| 2      | CIP     | Civil Infrastructure Platform             |
| 3      | SLTS    | Super Long Term Support                   |
| 4      | OTA     | Over The Air                              |
| 5      | SWG     | Security Work Group                       |
| 6      | IEC     | International Electrotechnical Commission |
| 7      | SBOM    | Software Bill Of Materials                |
| 8      | RT      | Real-Time                                 |
| 9      | CI      | Continous Integration                     |

### Process behind creating requirement traceability matrices <a name="process"></a>

The process flow to document the below traceability matrics is mentioned below:

1. CIP SWG members documented the functional and non-functional requirements in [here](./CIP_requirements.md) which is used while creating this traceability document.
2. While mapping the respective design and testing evidence to the finalized requirements, CIP SWG members had thorough discussions with CIP-Core WG and CIP Testing WG members to obtain respective evidence for the defined requirements.

The unfilled traceability data shall be documented after further discussion with respective WG members.

### Traceability matrix from CIP requirements to design <a name="reqtodesign"></a>

| Req ID           |  Requirement                                                                      |  CIP Design description                  |
|------------------|-----------------------------------------------------------------------------------|------------------------------------------|
| #REQ-CIP-FUNC-01 |  Reuse Linux mainline kernel, customise configs based on CIP members requirement | CIP Kernel WG members reuse mainline kernel and maintain it for [SLTS](https://git.kernel.org/pub/scm/linux/kernel/git/cip/linux-cip.git/). The kernel configurations required for CIP use cases are made and maintained in this [repository](https://gitlab.com/cip-project/cip-kernel/cip-kernel-config) for all supported versions. |
| #REQ-CIP-FUNC-02 | Provide CIP RT kernel by applying PREEMPT_RT patches  | CIP Kernel WG members also maintain the real-time versions of the Long term supported kernel branches for 10+ life period. Additional details regarding kernel maintenance are available in the [CIP user manual](https://gitlab.com/cip-project/cip-documents/-/blob/master/user/user_manual/user_manual.md?ref_type=heads#cip-kernel) document.  |
| #REQ-CIP-FUNC-03  | Develop meta-data to create minimal CIP reference images  | CIP developers consistently develop recipes to create CIP reference images for various architectures, Debian suites with supported Kernel versions along with additional features like software update, secure boot, security layer, data encrpytion etc. This meta-data is maintained in [isar-cip-core](https://gitlab.com/cip-project/cip-core/isar-cip-core) repository |
| #REQ-CIP-FUNC-04  | Support multiple cpu architectures in CIP reference images  | CIP currently supports [mentioned](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/tree/master/kas/board?ref_type=heads) architectures with their meta-data in their reference images. |
| #REQ-CIP-FUNC-05 | Support Secure boot | CIP implemented recipes to enable secure boot on their supported architectures. Additional details regarding Secure boot design in CIP is available in the [user manual](https://gitlab.com/cip-project/cip-documents/-/blob/trace_req_design_testing/user/user_manual/user_manual.md?ref_type=heads#secure-boot-feature) document. |
| #REQ-CIP-FUNC-06 | Support SWUpdate with local file and OTA | CIP SWupdate WG designed and developed recipes to enable local,over-the-air,signed and unsigned SWupdate. Swupdate package from Debian is used in their design to achieve this. Additional details about types of software updates available in CIP are mentioned in the [user manual](https://gitlab.com/cip-project/cip-documents/-/blob/master/user/user_manual/user_manual.md?ref_type=heads#cip-software-updates) document |
| #REQ-CIP-FUNC-07 |  Support SWUpdate with signed & encrypted images |  CIP WG designed and developed recipes to create encrypted reference images which are also simultaneously signed and can also be updated. |
| #REQ-CIP-FUNC-08 | Support security features based on IEC 624443-4-2 standard | CIP SWG added security related Debian packages in their design to fulfill security requirements based on IEC 62443-4-2. The list of security related packages which are added to harden the CIP IEC layer are mentioned in this [security hardening](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/CIP_Security_Hardening.md?ref_type=heads#4-technical-implementation-details) document.  |
| #REQ-CIP-FUNC-09 | Deliver a generatable SBOM along with the sample configuration | TODO: Need to discuss with CIP members |
| #REQ-CIP-NON-FUNC-01 | Follow upstream first policy for CIP Core and CIP Kernel development | TODO: Shall be updated in future |
| #REQ-CIP-NON-FUNC-02 | Maintain SLTS kernel for 10+ years | TODO: Shall be updated in future |
| #REQ-CIP-NON-FUNC-03 | Use Debian based packages or third party applications to create CIP Core reference images | CIP uses latest stable Debian packages according to their use cases to build the base system for the reference images. |
| #REQ-CIP-NON-FUNC-04 | Accept only kernel patches which are upstreamed | The patches are applied to CIP Kernel under the criteria that they are accepted first in upstream stable branches maintained by Greg Kroah Hartmann |

### Traceability matrix from CIP requirements to testing process <a name="reqtotesting"></a>

| Req ID            |    Requirement                                                                     |   CIP testing description        |
|-------------------|------------------------------------------------------------------------------------|----------------------------------|
| #REQ-CIP-FUNC-01  | Reuse Linux mainline kernel, customise configs based on CIP members requirement | Automated tests run in [LAVA Lab](https://lava.ciplatform.org/results/) and [Kernel CI](https://linux.kernelci.org/job/cip/) whenever changes are made to CIP Kernel.  |
| #REQ-CIP-FUNC-02 | Provide CIP RT kernel by applying PREEMPT_RT patches | Real-time kernels are also tested using automated test jobs implemented in LAVA lab and Kernel CI. |
| #REQ-CIP-FUNC-03 | Develop meta-data to create minimal CIP reference images | TODO: Discuss among CIP-Core WG members and finalize meta-data testing method |
| #REQ-CIP-FUNC-04 | Support multiple cpu architectures in CIP reference images | Whenever the metadata is modified, a CI runs to test the builds of reference images on all supported architectures. Here is a [sample result](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/pipelines/985520055) which shows the build test results on all supported architectures. |
| #REQ-CIP-FUNC-05 | Support Secure boot | Currently an [automated test case](https://gitlab.com/cip-project/cip-testing/cip-security-tests/-/merge_requests/13) is implemented in CIP IEC layer which verifies whether Secure boot is enabled or not for amd64, arm64 and armhf architectures.  |
| #REQ-CIP-FUNC-06 | Support SWUpdate with local file and OTA | The process to test SWupdate on CIP reference images is clearly documented in this [manual](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/blob/master/doc/README.swupdate.md?ref_type=heads). |
| #REQ-CIP-FUNC-07 | Support SWUpdate with signed & encrypted images | CIP-Core meta-data allows to build encrypted-signed reference images. So the steps mentioned in this [testing](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/blob/master/doc/README.swupdate.md) document can be used to test the SWupdate functionality.|
| #REQ-CIP-FUNC-08 | Support security features based on IEC 624443-4-2 standard | CIP SWG developed test scripts to test the security features of the Debian packages installed the security image. The steps to run these tests are clearly documented [here](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/blob/master/doc/README.security-testing.md)  |
| #REQ-CIP-FUNC-09 |  Deliver a generatable SBOM along with the sample configuration  | TODO: Shall be updated in future after discussion with CIP members |
| #REQ-CIP-NON-FUNC-01 | Follow upstream first policy for CIP Core and CIP Kernel development | TODO: Shall be updated in future |
| #REQ-CIP-NON-FUNC-02 | Maintain SLTS kernel for 10+ years | TODO: Shall be updated in future |
| #REQ-CIP-NON-FUNC-03 | Use Debian based packages or third party applications to create CIP Core reference images | CIP uses latest stable Debian packages according to their use cases to build the base system for the reference images. |
| #REQ-CIP-NON-FUNC-04 | Accept only kernel patches which are upstreamed | TODO: Shall be updated in future |
