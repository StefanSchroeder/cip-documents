
# CIP Development process (SM-1)

## Table of contents

1. [Introduction](#introduction)
2. [Scope](#Scope)
3. [SM-1 : Development process](#developmentprocess)
    - 3.1 [Configuration management with change controls](#conf_management)
    - 3.2 [Audit Logging](#auditlogging)
    - 3.3 [Requirements definition](#requirements)
    - 3.4 [CIP Design](#design)
    - 3.5 [CIP Implementation](#implementation)
    - 3.6 [Testing and validation](#testing)
    - 3.7 [Review and approval](#reviewandapproval)
    - 3.8 [Life-cycle](#lifecycle)

*****
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description                                            | Author       | Reviewed by                               |
|-------------|------------|---------------------------------------------------------------|--------------|-------------------------------------------|
| 001         | 2022-12-20 | Draft document of CIP development process                        | Sai Ashrith | Dinesh Kumar |
| 002         | 2023-03-28 | Removed package proposal flowchart harcopy and replaced it with a link   | Sai Ashrith | Dinesh Kumar     |
| 003         | 2023-06-21 | Add the stages of development process as seperate sections   |  Sai Ashrith |  Dinesh Kumar  |
****
<div style='page-break-after: always'></div>

## 1. Introduction <a name="introduction"></a>

This document explains the development process used in CIP based on documented requirement checklist for SM-1 requirement in 62443-4-1. Design details such as system's devices and subsystems connections, exploitable areas and trust boundaries based on SD-1 requirement in 62443-4-1 are also included in this document.

## 2. Scope <a name="Scope"></a>

This document explains the requirements which are clearly confirmed after discussion with Certification body. Some part of documentation is still not available due to need of further discussion with certification body such as configuration management document is SM-1.

## 3. SM-1 : Development process <a name="developmentprocess"></a>

The documentations and details required to fulfill SM-1 requirement in CIP are mentioned below :

### 3.1 Configuration Management with change controls <a name="conf_management"></a>

Configuration management document is available [here](./configuration_management.md).It has the details of versioning rules followed for CIP-Core and CIP-kernel.

### 3.2 Audit logging <a name="auditlogging"></a>

Audit logging procedure implemented in CIP while documenting the process related items as well during the development cycle is mentioned [here](https://gitlab.com/cip-project/cip-documents/-/blob/master/process/config_management_audit_logging.md#audit_logging).

### 3.3 Requirements definition <a name="requirements"></a>

The overview of CIP requirements details are mentioned [here](https://www.cip-project.org/wp-content/uploads/sites/17/2018/10/CIP_Whitepaper_10.19.18.pdf) as project goals.

   Some of the project goals of CIP are :-

   1. Super Long Term Support (SLTS) for **CIP kernel** and **CIP Core** packages up to 10+ years.
   2. Regular testing of security updates on [CIP reference](https://wiki.linuxfoundation.org/civilinfrastructureplatform/ciptesting/cipreferencehardware) boards.
   3. Provide CIP reference images to CIP users.
   4. Each workgroup to have it's own focus areas.

   CIP members made this [document](./CIP_requirements.md) where the functional and non-functional requirements are listed.

### 3.4 CIP Design <a name="design"></a>

**CIP-Core** re-uses **Debian** pre-built packages along with CIP-kernel which re-uses mainline kernel. The design and developments process which takes place in mainline kernel is mentioned [here](https://www.kernel.org/doc/html/latest/process/2.Process.html). CIP-Core design and development details are mentioned in this [CIP-Core Wiki page](https://wiki.linuxfoundation.org/civilinfrastructureplatform/cip-core) along with the [list of packages](https://gitlab.com/cip-project/cip-core/cip-pkglist) being used in the system.

Every package used from Debian in CIP-Core follows the process as mentioned in this [illustration](https://gitlab.com/cip-project/cip-core/cip-pkglist/-/blob/master/doc/pdp_workflow.jpg) before including in the design.

### 3.5 CIP Implementation <a name="implementation"></a>

**CIP-Core** implementation details are present in [CIP-Core Wiki page](https://wiki.linuxfoundation.org/civilinfrastructureplatform/cip-core) and the [isar-cip-core](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/tree/master) repository is actively maintained by the CIP developers. CIP kernel is actively maintained by the CIP developers in [CIP Kernel](https://gitlab.com/cip-project/cip-kernel/linux-cip) repository.CIP kernel mostly re-uses mainline Linux kernel, so here is the [reference](https://linux-kernel-labs.github.io/refs/heads/master/lectures/intro.html) with Linux kernel implementation details.

Here is an illustration which explains CIP-Core implementation :

  ![CIP-Core implementation](../resources/images/other_documents/cip-core-implementation.jpg)

### 3.6 Testing and validation <a name="testing"></a>

The Centralized testing in CIP developers can run tests without having support to a growing list of reference platforms.

Here is an illustration of testing overview in CIP. 

  ![CIP Testing Overview](../resources/images/user_manual/cip-testing-overview.png).

The continuous integration testing in CIP uses LAVA which helps to automatically test the updated software on CIP hardware. Kernel tests such as **Health and Meltdown checker**, **LTP tests** and some real time tests will run in the LAVA.

In the context of process for repeatable testing,

   * The detailed process of submitting a custom LAVA job is mentioned [here](https://wiki.linuxfoundation.org/civilinfrastructureplatform/ciptesting/centalisedtesting/submittinglavajobs).
   * The process of running IEC layer tests on the CIP security image is mentioned [here](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/blob/master/doc/README.security-testing.md).

**CIP-Core** relies on upstream testing as it re-uses Debian packages without any modifications. The rigorous testing process done by Debian is mentioned at [Debian Testing page](https://wiki.debian.org/DebianTesting).Debian also has it's own CI for testing Debian packages and the results of some packages are available [here](https://ci.debian.net/packages).

### 3.7 Review and approval <a name="reviewandapproval"></a>

This info needs to be discussed among CIP members and then should be documented.

### 3.8 Life-cycle <a name="lifecycle"></a>

The life-cycle details of **CIP-Core**, **CIP-Kernel** and their upstream projects are mentioned in [cip-lifecycle](https://gitlab.com/cip-project/cip-lifecycle) repository.
