# <Center>CIP Development Environment Security </Center>

# Table of contents
1. [Objective](#Objective)
2. [Assumptions](#Assumptions)
3. [Scope](#Scope)
4. [Security Requirement](#Security_Requirement)
5. [CIP Development Environment](#CIP_Dev_Env)

   5.1 [Protection During Development](#Protec_Dev)
   
   5.2 [Protection During Production](#Protec_Prod)
   
   5.3 [Protection During Delivery](#Protec_Delivery)
   
6. [Policy for CIP repository maintainer privilege](#CIP_policy_maintainer)
7. [Current CIP repositories and maintainers](#CIP_repo_maintainer)
8. [References](#references)

   ***** 
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description                                                | Author       | Reviewed by                               |
|-------------|------------|-------------------------------------------------------------------|--------------|-------------------------------------------|
| 001         | 2021-06-14 | Draft development environment security                            | Dinesh Kumar | To be reviewed by CIP Security WG members |
| 002         | 2021-06-19 | Updated based on Daniel-san's comment, email dated 15/06/2021     | Dinesh Kumar | To be reviewed by CIP Security WG members |
| 003         | 2021-08-30 | Updated based on Yasin's comment in Gitlab                        | Dinesh Kumar | Yasin                                     |
| 004         | 2022-04-14 | Reviewed and updated all Gitlab owners and maintainers            | Dinesh Kumar | TBR                                       |
| 005         | 2022-06-20 | Updated owners table                                              | Dinesh Kumar | BV members                                |
| 006         | 2024-05-29 | Added details for Gitlab provided protection based on BV feedback | Dinesh Kumar | George Hsiao                              |
| 007         | 2024-06-20 | Added gitlab security compliance based on George suggestion       | Dinesh Kumar | TBR                                       |

****
<div style='page-break-after: always'></div>

***

## 1. Objective <a name="Objective"></a>

The primary objective of this document is to explain current development environment security, development flow and how security is maintained during CIP platform development.

Moreover, subsequent revisions of this document may consider additional details of existing development or changes and enhancement to improve development environment security.

## 2. Assumptions <a name="Assumptions"></a>

| Assumption                               | Impact                                   |
|------------------------------------------|------------------------------------------|
| The development environment of upstream developers( Debian and Mainline kernel) is protected | Any security loopholes embedded in upstream project will impact CIP directly |


## 3. Scope <a name="Scope"></a>

Scope of this document is to consider current development model for CIP. Current CIP development model follows open source development method where everyone is allowed to contribute and only few members have privilege to merge the changes in CIP repositories.

This document is intended to meet CIP IEC-62443-4-1 SM-7 requirement which expects a process for securing CIP development environment.

## 4. Security Requirement <a name="Security_Requirements"></a>

IEC-62443-4-1 has following development environment security requirements

* CIP shall define process which has procedural as well as technical control for protecting a product during development,  
  production and delivery. This    
  includes following
  
  1. Update patches
  2. Design
  3. Implementation
  4. Testing
  5. Releases
   
* Having this process in place means CIP provides ways to protect   integrity of following
  
  1. Code
  2. Documents (User manuals, Design, )
  3. Configuration setting
  4. Private keys
  5. Authenticators (password, access control list, code signing certificates)

## 5. CIP Development Environment <a name="CIP_Dev_Env"></a>

### 5.1 Protection During Development <a name="Protec_Dev"></a>

As CIP re-uses open source components hence it is assumed upstream components are protected by respective component owner. However, artifacts such as various documents are kept in Gitlab and they are protected by Gitlab authentication mechanism. Similarly metadata of recipes(.bb, .bbclass files etc) is protected by Gitlab authentication mechanism.

GitLab has adopted the ISO/IEC 27001:2022, ISO/IEC 27017:2015 and ISO/IEC 27018:2019 standards for information security management system. Additionally Gitlab complies to several other security standards which are available at [Gitlab Trust centre](https://about.gitlab.com/security/).

During development any CIP developers can send merge requests with their changes. Here one thing to note is CIP developers can send merge requests, whereas CIP contributors can only send patches to the mailing list. All changes/patches are reviewed by CIP peer developers and feedback is provided. Once all the review comments are fixed, CIP maintainer of the respective repository merges the changes.

![CIP development flow](../resources/images/other_documents/CIP_Dev_Flow.png)

Gitlab provides two factor authentication, password protection, key based authentication for protecting all the code and repositories.

### 5.2 Protection During Production <a name="Protec_Prod"></a>

This requirement is not applicable to CIP as CIP is a software based component and no production is required.

### 5.3 Protection During Delivery <a name="Protec_Delivery"></a>

This requirement should be met by CIP users.

Currently there is no production by CIP. When CIP adds production of images there will be information added here. Currently CIP does only deliver source code via Gitlab. Therefore, CIP relies on security provided by Gitlab.

## 6. Policy for CIP repository maintainer privilege <a name="CIP_policy_maintainer"></a>

CIP has following policy for reviewing maintainers privilege to control CIP repositories

* If a CIP maintainer leaves CIP development and has not contributed in 6 months, his merge privilege is revoked in all CIP repositories and downgraded to developer privilege.
  
* All repositories are reviewed annually. If a maintainer has not contributed in 6 months, his maintainer privileges are revoked.
  
* Any CIP member can be given maintainer rights for any repositories after TSC members approval.

CIP has following policy for reviewing maintainers privilege to control CIP AWS accounts

* If a CIP contributor on AWS leaves CIP development and has not contributed in 6 months, his AWS account access is revoked.
  
* All AWS accounts are reviewed annually. If a contributor has not contributed in 6 months, his access is revoked if he left his role.
  
* Any CIP member can be given AWS access rights after TSC members approval.

* No shared machine accounts are allowed to have remote login access enabled. (Feedback needed)

## 7. Current CIP repositories and maintainers <a name="CIP_repo_maintainer"></a>

| CIP Gitlab group | Repo and it's URL | Owners | Maintainers | Last review date |
|----|----|----|----|----|
| cip-project      | [cip-project](https://Gitlab.com/cip-project)                                            | Yoshitake Kobayashi, Chris Paterson, Takehisa Katayama, Hidehiro Kawai, Laurence Urhegyi, Kazuhiro Hayashi, Samuel holder, Kento Yoshida, Jan Kiszka                | None                                                                                   | 20 Jun 2022      |
| cip-security     | [iec_62443-4-x](https://Gitlab.com/cip-project/cip-security/iec_62443-4-x) | Hidehiro Kawai,Kento Yoshida, Laurence Urhegyi, Kazuhiro Hayashi, Yasin Demirci, Samuel holder,Jan Kiszka, Chris Paterson,Takehisa Katayama, Yoshitake Kobayashi    | Stefan shroeder                                                                        | 14 Apr 2022      |
| cip-sw-update    | [cip-sw-updates-demo](https://Gitlab.com/cip-project/cip-sw-updates/cip-sw-updates-demo)<br><br>[swupdate-handler-roundrobin](https://Gitlab.com/cip-project/cip-sw-updates/swupdate-handler-roundrobin)<br><br>[cip-sw-updates-tasks](https://Gitlab.com/cip-project/cip-sw-updates/cip-sw-updates-tasks) | Hidehiro Kawai,Kento Yoshida, Laurence Urhegyi, Kazuhiro Hayashi, Samuel holder,Jan Kiszka, Chris Paterson,Takehisa Katayama, Yoshitake Kobayashi, Akihiro Suzuki   | swupdate-handler-roundrobin maintainer Christian Storm<br><br>                         | 14 Apr 2022      |
| cip-core         | [isa-cip-core](https://Gitlab.com/cip-project/cip-core/isar-cip-core)  <br><br>[deby](https://Gitlab.com/cip-project/cip-core/deby) <br><br>[cip-pkglist](https://Gitlab.com/cip-project/cip-core/cip-pkglist) | Hidehiro Kawai,Kento Yoshida, Laurence Urhegyi, Kazuhiro Hayashi, Samuel holder,Jan Kiszka, Chris Paterson,Takehisa Katayama, Yoshitake Kobayashi, Daniel Sangorrin | Alice Ferrazzi                                                                         | 14 Apr 2022      |
| cip-kernel       | [cip-kernel-sec](https://Gitlab.com/cip-project/cip-kernel/cip-kernel-sec)<br><br>[linux-cip](https://Gitlab.com/cip-project/cip-kernel/linux-cip) <br><br>[cip-kernel-config](https://Gitlab.com/cip-project/cip-kernel/cip-kernel-config) <br><br>[lts-commit-list](https://Gitlab.com/cip-project/cip-kernel/lts-commit-list)<br><br>[cip-kernel-tests](https://Gitlab.com/cip-project/cip-kernel/cip-kernel-tests)<br><br>[classify-failed-patches](https://Gitlab.com/cip-project/cip-kernel/classify-failed-patches)<br> | Hidehiro Kawai,Kento Yoshida, Laurence Urhegyi, Kazuhiro Hayashi, Samuel holder,Jan Kiszka, Chris Paterson,Takehisa Katayama, Yoshitake Kobayashi                   | cip-kernel-sec: Nobohiro Iwamatsu, Pavel Machek, Masami Ishikawa<br><br>linux-cip, cip-kernel-config, classify-failed-patches: Nobohiro Iwamatsu, Pavel Machek<br><br>lts-commit-list: Nobohiro Iwamatsu, Pavel Machek, Ulrich Hecht<br><br>cip-kernel-tests: Robert Marshal, Nobohiro Iwamatsu, Pavel Machek<br><br> | 14 Apr 2022      |
| cip-testing      | [cip-testing](https://Gitlab.com/cip-project/cip-testing )           | Hidehiro Kawai,Kento Yoshida, Laurence Urhegyi, Kazuhiro Hayashi, Samuel holder,Jan Kiszka, Chris Paterson,Takehisa Katayama, Yoshitake Kobayashi                   | Alice Ferrazzi                                                                         | 14 Apr 2022      |
| cip-lifecycle    | [cip-lifecycle](https://Gitlab.com/cip-project/cip-lifecycle)        | Hidehiro Kawai,Kento Yoshida, Laurence Urhegyi, Kazuhiro Hayashi, Samuel holder,Jan Kiszka, Chris Paterson,Takehisa Katayama, Yoshitake Kobayashi                   | None                                                                                   | 14 Apr 2022      |
| cip-documents    | [cip-documents](https://Gitlab.com/cip-project/cip-documents)         | Hidehiro Kawai,Kento Yoshida, Laurence Urhegyi, Kazuhiro Hayashi, Samuel holder,Jan Kiszka, Chris Paterson,Takehisa Katayama, Yoshitake Kobayashi                   | Dinesh Kumar                                                                           | 14 Apr 2022      |

## 8. References <a name="references"></a>

1. Gitlab Compliance features
   
   https://about.gitlab.com/blog/2022/07/13/top-5-compliance-features-to-leverage-in-gitlab/