 # <Center>CIP Security Requirements</Center>

# Table of contents
1. [Overview](#Overview)
2. [IEC-62443-4-2 Requirements](#IEC-62443-4-2_Requirements)
3. [Other Security Requirements](#Other_Security_Requirements)

    3.1 [File Integrity](#File_Integrity)
    
    3.2 [Development Environment Security](#Develop_Env_Sec)
    
    3.3 [Private Key Protection](#Private_key_Protection)
    
    3.4 [CIP Core CVE Tracking](#CIP_Core_CVE_Tracking)
    
    3.5 [CIP Kernel CVE Tracking](#CIP_Kernel_CVE_Tracking)
    
    3.6 [Security Level](#Security_Level)
    
    3.7 [Security Updates](#Security_Updates)
    
    3.8 [System Hardening](#System_Hardening)
    
    3.9 [Default User Accounts](#Default_User_Accounts)
    
    3.10 [Security Context](#Security_Context)
    
   ***** 
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description                                        | Author       | Reviewed by |
|-------------|------------|-----------------------------------------------------------|--------------|-------------|
| 001         | 2021-02-24 | Draft security requirement based on IEC-62443-4-x created | Dinesh Kumar | TBD         |
| 002         | 2023-07-10 | Updated requirement IDs                                   | Dinesh Kumar | TBR         |





****
<div style='page-break-after: always'></div>

















<div style='page-break-after: always'></div>

## Overview <a name="Overview"></a>

This document is intended to capture CIP security requirements based on IEC-62443-4-2 standard. In future this document should be revised based on additional security requirements.

## IEC-62443-4-2 Requirements <a name="IEC-62443-4-2_Requirements"></a>

Following table outlines CIP security requirements. These requirements are derived from IEC-62443-4-2 security requirements which could be applicable to CIP.

Each requirement has been assigned one unique identifier as requirement ID e.g. CIP_SEC_IEC_FUNC_REQ_1 which can be interpreted as <CIP WG>_<source of req>_<Functional or non-functional>_<Identifier>

| Requirement ID | Requirement | Description | Source of requirement |
|----|----|-------|----|
| #REQ-CIP-SEC-IEC-FUNC-REQ_1  | Support human user identification & authentication, user account management, password management | Following are the key requirements<br>1. System should support identification and authentication for all users at all accessible interfaces, either locally or by integration into a system.<br>2.  System should be able to uniquely identify and authenticate all human users<br>3. System should support  account management functions in order to make changes such as activate, modify, disable and remove user accounts<br>4. System should support default authenticator change<br>5. System should support changing password once expired | IEC-62443-4-2 CR1.1, CR1.1(1), CR1.3, CR1.5, CR1.7(1), CR1.7(2)                            |
| #REQ-CIP-SEC-IEC-FUNC-REQ_2  | Limit number of unsuccessful login attempts, limit on concurrent sessions                        | 1. System should have provision to limit number of unsuccessful login attempts and once unsuccessful login attempts exceed, the account should be locked either temporarily for some time or permanently and admin user should be able to unlock it.<br>2. System should limit number of concurrent sessions from any user(human, process or device) to prevent DoS attack                                                                                                                                                                        | IEC-62443-4-2 CR1.11, CR2.7                                                                |
| #REQ-CIP-SEC-IEC-FUNC-REQ_3  | Multifactor authentication for all interfaces for human users                                    | System should provide multifactor authentication for human users on all interfaces                                                                                                                                                                                                                                                                                                                                                                                                                                                                | IEC-62443-4-2 CR1.1(2)                                                                     |
| #REQ-CIP-SEC-IEC-FUNC-REQ_4  | Unique identifiers for identifying each account                                                  | System should provide capability to integrate into a system that supports function of generating unique identifiers                                                                                                                                                                                                                                                                                                                                                                                                                               | IEC-62443-4-2 CR1.5(1), CR1.9(1), CR1.14(1)                                                |
| #REQ-CIP-SEC-IEC-FUNC-REQ_5  | Capability to access secure storage for cryptographic keys/authenticators                        | System should provide capability to access secure hardware or TPM to meet following requirements<br>1. Access authenticators stored in secure HW<br>2. Symmetric keys stored in secure HW<br>3. Private keys stored in secure HW                                                                                                                                                                                                                                                                                                                  | IEC-62443-4-2 CR1.5(1), CR1.9(1), CR1.14(1)                                                |
| #REQ-CIP-SEC-IEC-FUNC-REQ_6  | PKI(Public Key Infrastructure) support, use of cryptography                                      | System should support following functionality <br>1. Usage of PKI for authentication<br>2. Authenticator feedback<br>3. Strong symmetric key based authentication<br>4. All communication protected by integrity and authentication<br>5. Information confidentiality by applying encryption<br>6. Key management according to NIST SP 800-57<br>7. Usage of cryptographic algorithms recognized by international standards                                                                                                                       | IEC-62443-4-2 CR1.8, CR1.9, CR1.10, CR1.14, CR3.1, CR3.1(1), CR3.4, CR3.4(1), CR4.1, CR4.3 |
| #REQ-CIP-SEC-IEC-FUNC-REQ_7  | Authorization enforcement support                                                                | System should support following functionality<br>1. Authorization enforcement for all authenticated and identified users<br>2. Usage of control policies like identity based policies, role based policies and rule based policies                                                                                                                                                                                                                                                                                                                | IEC-62443-4-2 CR2.1, CR2.1(1), CR2.1(2), CR3.9                                             |
| #REQ-CIP-SEC-IEC-FUNC-REQ_8  | Protection of audit information                                                                  | System should support protection of audit information, audit logs and all the audit tools from unauthorized access, modifications, deletions                                                                                                                                                                                                                                                                                                                                                                                                      | IEC-62443-4-2 CR3.9                                                                        |
| #REQ-CIP-SEC-IEC-FUNC-REQ_9  | Supervisor override                                                                              | System should support functionality to temporarily elevate privileges of a normal user to higher level, this should be recorded and controllable                                                                                                                                                                                                                                                                                                                                                                                                  | IEC-62443-4-2 CR2.1(3)                                                                     |
| #REQ-CIP-SEC-IEC-FUNC-REQ_10 | Session lock and remote session termination                                                      | System should provide following functionality in order to protect user sessions<br>1. Unattended open sessions should get locked after a configurable time period and either user or admin should be able to unlock<br>2. Terminate remote session after configurable period of time                                                                                                                                                                                                                                                              | IEC-62443-4-2 CR2.5, CR2.6                                                                 |
| #REQ-CIP-SEC-IEC-FUNC-REQ_11 | Auditable events                                                                                 | System should support following functionality for auditable events<br>1. Create audit records for security events such as access control, request errors, control system events, backup-restore events, configuration changes, audit log events<br>2. Each record should have timestamp, source, event id, event results<br>3. Automated notification of integrity violation                                                                                                                                                                      | IEC-62443-4-2 CR2.8, CR2.10, CR2.12, CR2.12(1)                                             |
| #REQ-CIP-SEC-IEC-FUNC-REQ_12 | Audit storage                                                                                    | System should support following functionality for audit storage<br>1. Allocate audit record storage according to recommended size<br>2. Mechanism to protect against failure of component when it reaches audit storage limit<br>3. Give warning when audit record storage capacity threshold reached                                                                                                                                                                                                                                             | IEC-62443-4-2 CR2.9, CR2.9(1)                                                              |
| #REQ-CIP-SEC-IEC-FUNC-REQ_13 | Software and information integrity                                                               | System should provide following functionality to support this requirement<br>1. Capability to detect authenticity of software and information<br>2. Report integrity violations in an automated fashion                                                                                                                                                                                                                                                                                                                                           | IEC-62443-4-2 CR3.4(1), CR3.4(2)                                                           |
| #REQ-CIP-SEC-IEC-FUNC-REQ_14 | Denial of service protection                                                                     | System should support mitigation for DoS attacks and make sure essential services are kept intact                                                                                                                                                                                                                                                                                                                                                                                                                                                 | IEC-62443-4-2 CR7.1(1)                                                                     |
| #REQ-CIP-SEC-IEC-FUNC-REQ_15 | Control system backup and recovery                                                               | System should support following functions<br>1. Capability to support system level backup including system and user state without affecting normal operation<br>2. Backup information should be encrypted in order to safeguard it, backup information should not store encryption keys instead encryption keys should be backed up separately<br>3. The integrity check of back up data should be supported                                                                                                                                      | IEC-62443-4-2 CR7.3, CR7.3(1)                                                              |
| #REQ-CIP-SEC-IEC-FUNC-REQ_16 | Timestamp and time synchronization                                                               | System should support following functions<br>1. Capability to create timestamps that can be used in logs, audit records and other required places<br>2. Timestamps are synchronized system wide with a common source<br>3. Time synchronization mechanism should be protected in such a way that any alteration could be detected                                                                                                                                                                                                                 | IEC-62443-4-2 CR2.11, CR2.11(1)                                                            |
| #REQ-CIP-SEC-IEC-FUNC-REQ_17 | Information persistence                                                                          | System should support following functions<br>1. Capability to erase information, for which explicit read authorization is supported<br>(aka factory reset)                                                                                                                                                                                                                 | IEC-62443-4-2 CR4.2                                                           |



## Other Security Requirements <a name="Other Security Requirements"></a>

### File Integrity <a name="File Integrity"></a>

| Requirement ID | Requirement | Description | Source of requirement |
|----|----|----|----|
| #REQ-CIP-SEC-IEC-NON-FUNC-REQ_1 | Integrity of CIP source code, scripts, executable | Receiver of CIP shall be able to verify integrity of scripts, source code and executable | IEC-62443-4-1 SM-6    |


### Development Environment Security <a name="Develop Env Sec"></a>

| Requirement ID | Requirement | Description | Source of requirement |
|----|----|----|----|
| #REQ-CIP-SEC-IEC-NON-FUNC-REQ_2 | Development environment security | There shall be a process to ensure protection of product during, development, production and delivery including software updates during design, implementation, testing and release | IEC-62443-4-1 SM-7    |


### Private Key Protection <a name="Private key Protection"></a>

| Requirement ID | Requirement | Description | Source of requirement |
|----|----|----|----|
| #REQ-CIP-SEC-IEC-NON-FUNC-REQ_3 | Protection of private keys | There shall be a process to protect all private keys used for code signing from unauthorized access or modifications | IEC-62443-4-1 SM-8    |


### CIP Core CVE Tracking <a name="CIP_Core_CVE_Tracking"></a>
**#REQ-CIP-SEC-IEC-NON-FUNC-REQ_4**

CIP Core should define and implement CVE tracking methods to incorporate latest fixes for CVEs

### CIP Kernel CVE Tracking <a name="CIP_Kernel_CVE_Tracking"></a>

**#REQ-CIP-SEC-IEC-NON-FUNC-REQ_5**

CIP Kernel should define and implement CVE tracking methods to incorporate latest fixes for CVEs

### Security Level <a name="Security_Level"></a>

**#REQ-CIP-SEC-IEC-NON-FUNC-REQ_6**

CIP targets to achieve SL-3 by adding required security features and configurations. However, it depends upon final recommendation from Certification Body, what could be the Security Level.

### Security Updates <a name="Security_Updates"></a>

**#REQ-CIP-SEC-IEC-NON-FUNC-REQ_7**

CIP should apply latest security fixes published through CVEs.

### Default User Accounts <a name="Default_User_Accounts"></a>

**#REQ-CIP-SEC-IEC-NON-FUNC-REQ_8**

CIP should publish default user accounts and their privileges.

### Security Context <a name="Security_Context"></a>

**#REQ-CIP-SEC-IEC-NON-FUNC-REQ_9**

Product owners shall define product security context as expected in IEC-62443-4-1 SR-1.

The security context shall describe:

* scope of the product
* a system overview (including high level breakdown of components)
* description of the intended operational environment
* assumptions about the product and the environment and constraints of the product
* expected measures to be available for threat mitigation
* the targeted network zone
* physical protection requirements and assumptions
* recommended firewall setup

The purpose of the defined security setup is to ensure that product developers and 
product users have a shared understanding of the security expectations in the
intended operational environment and to ensure that product users understand the
security implications of the product's exposure within its ecosystem.


