# <Center>CIP User Manual</Center>

## Table of contents
1. [Overview](#Overview)
2. [Civil Infrastructure Platform](#Civil_Infrastructure_Platform)
3. [Reference Hardware](#Reference_hardware)
4. [Open Source Base Layer](#Open_Source_Base_Layer)
   - 4.1 [CIP Kernel](#cip_kernel)
   - 4.2 [CIP Core](#cip_core)
5. [How to Create CIP Images](#How_to_create_CIP_images)
6. [Default User Accounts](#Default_User_Accounts)
7. [Porting CIP for New Hardware](#Porting_CIP_for_new_hardware)
8. [CIP Testing](#CIP_testing)
9. [CIP Software Updates](#CIP_Software_Updates)
10. [CIP System Monitoring](#CIP_System_Monitoring)
11. [References](#References)

*****
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description                                            | Author       | Reviewed by                               |
|-------------|------------|---------------------------------------------------------------|--------------|-------------------------------------------|
| 001         | 2021-02-12 | Draft document for user manual   | Shivanand Kunijadar |                    |
| 002         | 2023-10-04 | Added more information about CIP Kernel and some meta-data features like Secure boot, Swupdate etc. | Sai Ashrith  |   |

### 1. Overview <a name="Overview"></a>

This document briefly explains all the components in CIP such as its core components, Kernel, current reference hardware which supports CIP reference images, additionally provided features such as secure boot, Data encryption at rest etc. 

### 2. Civil Infrastructure Platform <a name="Civil_Infrastructure_Platform"></a>

The Civil Infrastructure Platform (“CIP”) is a collaborative, open source project hosted by the Linux Foundation. The CIP project is focused on establishing an open source “base layer” of industrial grade software to enable the use and implementation of software building blocks in civil infrastructure projects. Currently, civil infrastructure systems are built from the ground up, with little re- use of existing software building blocks.

The CIP project intends to create reusable building blocks that meet the safety, reliability and other requirements of industrial and civil infrastructure.
CIP works closely with the upstream community and does not aim to create a new Linux distribution. 

### Reference Hardware <a name="Reference_hardware"></a>

The CIP project has selected a number of hardware platforms to be used as reference platforms for the project's software.

Refer the below link for CIP project reference hardwares
https://wiki.linuxfoundation.org/civilinfrastructureplatform/ciptesting/cipreferencehardware

### Open Source Base Layer (OSBL) <a name="Open_Source_Base_Layer"></a>
OSBL is a set of industrial grade core open source software components,
tools and methods. It is composed of the CIP kernel source code, and the CIP Core source packages. 

![](../../resources/images/user_manual/OSBL.png)

#### CIP Kernel <a name="cip_kernel"></a>
CIP supports and maintains the kernel for a long time (+10 years). 

Refer the below link for CIP kernel maintenance
https://wiki.linuxfoundation.org/civilinfrastructureplatform/cipkernelmaintenance

#### Currently supported CIP SLTS kernels

The current released CIP kernels are as follows.

|Version	|Maintainer(s)|	First Release	|Projected EOL|
|:-----------|:-----------|:-----------|:-----------| 
|SLTS v6.1 | Nobuhiro Iwamatsu & Pavel Machek | 2023-07-14 | 2033-08 |
|SLTS v6.1-rt | Pavel Machek | 2023-07-16 | 2033-08 |
|SLTS v5.10 | Nobuhiro Iwamatsu & Pavel Machek | 2021-12-05 | 2031-01 |
|SLTS v5.10-rt | Pavel Machek | 2021-12-08 | 2031-01 |
|SLTS v4.19	|Nobuhiro Iwamatsu & Pavel Machek	|2019-01-11	|2029-01|
|SLTS v4.19-rt|	Pavel Machek	|2019-01-11	|2029-01|
|SLTS v4.4	|Nobuhiro Iwamatsu & Pavel Machek|	2017-01-17	|2027-01|
|SLTS v4.4-rt|	Pavel Machek	|2017-11-16	|2027-01 |

#### CIP core <a name="cip_core"></a>
CIP core provides example file system images using available build and image generation tools. It focuses on user land software and tools.
Currently, CIP is using [meta-debian](https://github.com/meta-debian/meta-debian) for Deby, and [ISAR](https://github.com/ilbers/isar) for isar-cip-core.

![](../../resources/images/user_manual/minimum-base-system.png)

CIP core has two profiles:

- The tiny profile is built from Debian source code and is useful for devices with storage restrictions, extreme performance and flexibility requirements, and low-complexity applications.

- The generic profile is built from Debian binary packages and covers devices that require more functionality, have less performance and flexibility requirements, and more storage.

![](../../resources/images/user_manual/cip-core-profiles.PNG)

#### Tiny profile(Deby)
 
Deby is built with poky and meta-debian, a layer for the poky build system that allows cross-building file system images from Debian source packages. Deby does not use Yocto/OE source code.

![](../../resources/images/user_manual/deby-core.png)

Refer Deby CIP source repository at https://gitlab.com/cip-project/cip-core/deby 

#### Generic Profile (isar-cip-core)
ISAR uses bitbake to generate the file system image by reusing Debian binaries and rebuilding packages that need modifications for the target board. 

![](../../resources/images/user_manual/isar-elbe.png)

Refer ISAR CIP source repository at https://gitlab.com/cip-project/cip-core/isar-cip-core

#### Secure boot feature
CIP aims to provide integrity check of the image during boot using the Secure boot mechanism. Necessary keys and certificates can be manually generated or can be directly used from the meta-data(For ex: snakeoil keys) to sign and verify the signatures during boot process.

Currently the Secure boot design is not uniform for all the supported architectures. More detailed information about Secure boot design in CIP is available in this [manual](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/blob/master/doc/README.secureboot.md) documented in isar-cip-core.

#### Package list

The list of CIP Core packages and the process to add or remove packages is described [here](https://gitlab.com/cip-project/cip-core/cip-pkglist).

### How to Create CIP Images <a name="How_to_create_CIP_images"></a>
#### ISAR CIP Core 
Source repository:   
https://gitlab.com/cip-project/cip-core/isar-cip-core     

Refer below link for build steps of ISAR CIP core      
https://gitlab.com/cip-project/cip-core/isar-cip-core/-/blob/master/README.md  

#### Deby CIP Core 
Source repository:  
https://gitlab.com/cip-project/cip-core/deby    

Refer below link for build steps of Deby CIP core    
https://gitlab.com/cip-project/cip-core/deby/-/blob/cip-core-buster/README.md

### Default User Accounts <a name="Default_User_Accounts"></a>
Default user account is root. 
 
### Porting CIP for New Hardware <a name="Porting_CIP_for_new_hardware"></a>

**TBD**

### CIP Testing <a name="CIP_testing"></a>

- CIP uses B@D (Board at Desk) to run automated testing on local Beaglebone Black or Renesas RZ/G1M iwg20m platform.  

- CIP's centralised testing can run tests without having local access to a platform and it is useful as list of reference platforms grows.

- CIP also uses Continuous Integration (CI) testing to automatically test CIP software on CIP hardware.

The block diagram below provides an overview of CIP's centralised test infrastructure.

![](../../resources/images/user_manual/cip-testing-overview.png)

#### LAVA Testing

CIP have set up their own instance of LAVA (Linaro Automated Validation Architecture). LAVA is a continuous integration system for deploying operating systems onto physical and virtual hardware for running tests.

Refer below link for more information on LAVA testing.   
https://wiki.linuxfoundation.org/civilinfrastructureplatform/ciptesting/centalisedtesting/ciplava

#### Kernel CI Testing
**yet to update**

#### CIP Core Testing
**yet to update**

### CIP Software Updates <a name="CIP_Software_Updates"></a>

CIP aims to provide super long term support and it is important for CIP to have a reference software update mechanism.

Current CIP software update uses [Hawkbit](https://github.com/eclipse/hawkbit) server to store the swupdate related files. Client uses  [SWUpdate](https://github.com/sbabic/swupdate) and librsync to communicate with Hawkbit.

It supports the following functions

Image types (You have to select 1 type)
- raw update
- binary delta update using librsync

Security options (You can enable both of them at the same time)
- signed update
- encrypted update

Refer below link for more details on software updates
https://gitlab.com/cip-project/cip-documents/-/blob/master/event/2019/sw_updates_wg_mini-summit.pdf


### CIP System Monitoring <a name="CIP_System_Monitoring"></a>
**Yet to update** 

#### Monitoring System Logs

#### IPS & IDS Component Logs 

### References <a name="References"></a>

CIP Web site: https://www.cip-project.org
• CIP Wiki: https://wiki.linuxfoundation.org/civilinfrastructureplatform/  
• CIP source code  
• CIP GitLab: http://www.gitlab.com/cip-project  

Hawkbit: https://www.eclipse.org/hawkbit/
SWUpdate: https://sbabic.github.io/swupdate/index.html
LAVA: https://lava.ciplatform.org/
