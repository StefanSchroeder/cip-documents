# CIP (Civil Infrastructure Platform) Glossary

This document aims to maintain CIP glossary at one common place. There are multiple abbreviations used
in CIP workgroups, hence this list will  act as reference for CIP users and developers. Additionally CIP users should refer global [Debian glossary](https://wiki.debian.org/Glossary) for more details

**CIP**

`Civil Infrastructure Platform`

**CVE**
 
`Common Vulnerabilities and Exposures`

**DCO**

`Developer Certificate of Origin`, The CIP Project uses the Linux Foundation Developer Certificate of Origin (DCO). 
Contributors to the CIP Project should adhere to the Linux Foundation's DCO and include a sign-off in their contributions.

**DM**

`Defect Management`

**IEC**

`International Electrotechnical Commission`

**IDS**

`Intrusion Detection System`

**IPS**

`Intrusion Prevention System`

**LF**

`Linux Foundation`, an umbrella organization for several Open Source projects. CIP is hosted under LF

**OSBL**

`Open Source Base Layer`, a layer of minimal package list maintained by CIP for SLTS period.
  
**OTA**

`Over The Air`, providing software updates via radio, bluetooth, Wifi, or similar mechanisms.

**SD**

`Secure by Design`

**SG**

`Security Guidelines`

**SI**

`Secure Implementation`

**SLTS**

`Super Long Term Support`. CIP claims to maintain kernel for 10+ years.

**SM**

`Security Management`

**SR**

`Security Requirement`

*SUM*

`Security Update Management`

**SVV**

`Security Verification and Validation`

**SWG**

 `Security Working group`
  
**TSC**

`Technical Steering Committee`


## References

[1] CIP Wiki page
    https://wiki.linuxfoundation.org/civilinfrastructureplatform/start
    
[2] Debian Glossary
    https://wiki.debian.org/Glossary
